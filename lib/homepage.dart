import 'package:flutter/material.dart';
import 'package:basic_ui/drawer.dart';

class MyHomePage extends StatelessWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.indigo,
        centerTitle: true,
        title: Image.asset('assets/images/logo.png', fit: BoxFit.contain, height: 32),
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.notifications_none_outlined),
            onPressed: () {
              // Add onPressed action
            },
          ),
        ],
      ),
      backgroundColor: Colors.indigo,
      body: Column(
        children: [
          const SizedBox(height: 16),
          Row(
            children: const[
              Padding(padding: EdgeInsets.only(left: 20)),
              CircleAvatar(backgroundImage: AssetImage('assets/images/profile.png')),
              SizedBox(width: 10),
              Text('Marico User', style: TextStyle(fontSize: 18, color: Colors.white)),
            ],
          ),
          const SizedBox(height: 16),
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: InkWell(
              onTap: () {
                // Add onTap action
              },
              child: Container(
                padding: const EdgeInsets.all(12),
                decoration: BoxDecoration(
                  color: Colors.green,
                  borderRadius: BorderRadius.circular(8),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: const[
                    Expanded(child: Text('Start of the day', style: TextStyle(color: Colors.white, fontSize: 20), textAlign: TextAlign.center,)),
                    Icon(
                      Icons.arrow_forward_ios,
                      color: Colors.white,
                    ),
                  ],
                ),
              ),
            ),
          ),
          Container(
  margin: const EdgeInsets.all(16),
  decoration: BoxDecoration(
    color: Colors.indigo[800],
    borderRadius: BorderRadius.circular(8),
  ),
  child: Padding(
    padding: const EdgeInsets.all(16),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const Center(
          child: Text(
            'Transactions',
            style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.bold,
              color: Colors.white,
            ),
          ),
        ),
        const SizedBox(height: 16),
        Row(
          children: [
            Expanded(
              child: Column(
                children: const [
                  Text(
                    '100000',
                    style: TextStyle(fontSize: 16, color: Colors.white),
                  ),
                  SizedBox(height: 8),
                  Text(
                    '5000000',
                    style: TextStyle(fontSize: 16, color: Colors.white),
                  ),
                  SizedBox(height: 8),
                  Text(
                    'Achievement',
                    style: TextStyle(fontSize: 16, color: Colors.white),
                  ),
                ],
              ),
            ),
            Expanded(
              child: Column(
                children: const [
                  Text(
                    '38',
                    style: TextStyle(fontSize: 16, color: Colors.white),
                  ),
                  SizedBox(height: 8),
                  Text(
                    '50',
                    style: TextStyle(fontSize: 16, color: Colors.white),
                  ),
                  SizedBox(height: 8),
                  Text(
                    'EC',
                    style: TextStyle(fontSize: 16, color: Colors.white),
                  ),
                ],
              ),
            ),
          ],
        ),
        const SizedBox(height: 8),
        const Center(
          child: Text(
            'View Transactions >',
            style: TextStyle(fontSize: 14, color: Colors.white),
          ),
        ),
      ],
    ),
  ),
),

          Container(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: InkWell(
              onTap: () {
                // Add onTap action
              },
              child: Container(
                padding: const EdgeInsets.all(12),
                decoration: BoxDecoration(
                  color: Colors.indigo[800],
                  borderRadius: BorderRadius.circular(8),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: const [
                    Expanded(child: Text('End of the day', style: TextStyle(color: Colors.white, fontSize: 20), textAlign: TextAlign.center,)),
                    Icon(
                      Icons.arrow_forward_ios,
                      color: Colors.white,
                    ),
                  ],
                ),
              ),
            ),
          ),
          const SizedBox(height: 18,),
          Align(
            alignment: Alignment.centerLeft,
            child: Container(
              margin: const EdgeInsets.only(left: 20),
              child: const Text('Geo Adherence', style: TextStyle(fontSize: 22, color: Colors.white, fontWeight: FontWeight.bold),))),
          Container(
            margin: const EdgeInsets.all(16),
            decoration: BoxDecoration(
              color: Colors.indigo[700],
              borderRadius: BorderRadius.circular(8),
            ),
            child: Padding(
              padding: const EdgeInsets.all(14),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                        child: Container(
                          decoration: BoxDecoration(borderRadius: BorderRadius.circular(8), color: Colors.white),
                        margin: const EdgeInsets.all(10),
                        height: 80,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text('12/30', style: TextStyle(fontSize: 22, color: Colors.indigo[700], fontWeight: FontWeight.bold)),
                            const SizedBox(height: 8),
                            Text('Geo Adhered', textAlign: TextAlign.center, style: TextStyle(fontSize: 16, color: Colors.indigo[700])),
                          ],
                        ),
                        ),
                      ),
                    Expanded(
                      child: Container(
                        margin: const EdgeInsets.all(8),
                        height: 80,
                        decoration: BoxDecoration(borderRadius: BorderRadius.circular(8), color: Colors.white),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text('12/30', style: TextStyle(fontSize: 22, color: Colors.indigo[700], fontWeight: FontWeight.bold)),
                            const SizedBox(height: 8),
                            Text('Non Geo Adhered',textAlign: TextAlign.center , style: TextStyle(fontSize: 16, color: Colors.indigo[700])),
                          ],
                        ),
                      ),
                    ),
                    Expanded(
                      child: Container(
                        margin: const EdgeInsets.all(8),
                        decoration: BoxDecoration(borderRadius: BorderRadius.circular(8), color: Colors.white),
                        height: 80,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text('12/30', style: TextStyle(fontSize: 22, color: Colors.indigo[700], fontWeight: FontWeight.bold)),
                            const SizedBox(height: 8),
                            Text('Not Visited', style: TextStyle(fontSize: 16, color: Colors.indigo[700])),
                          ],
                        ),
                      ),
                    ),
              ],
        )])))
        ],
      ),
      drawer: const AppDrawer(),
      floatingActionButton: Container(
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          border: Border.all(width: 3, color: Colors.white),
        ),
        child: FloatingActionButton(
          backgroundColor: Colors.green[500],
          onPressed: () {
            // Add onPressed action
          },
          child: Icon(Icons.dashboard, size: 38,),
        ),
      )
    );
  }
}
