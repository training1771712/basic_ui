import 'package:flutter/material.dart';

class AppDrawer extends StatelessWidget {
  const AppDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: <Widget>[
          UserAccountsDrawerHeader(
            decoration: BoxDecoration(color: Colors.indigo, borderRadius: BorderRadius.circular(20)),
            accountName: const Text("Marico User"),
            accountEmail: const Text("User Designation"),
            currentAccountPicture: CircleAvatar(
              backgroundImage: const AssetImage('assets/images/profile.png'),
              child: Stack(
                children: [
                Positioned(
                  bottom: 0,
                  right: 0,
                  child: CircleAvatar(
                    backgroundColor: Colors.white, radius: 15,
                    child: Icon(Icons.edit, size: 22, color: Colors.indigo[800]),
                  ),
                ),
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal:14),
            child: ListTile(
              iconColor: Colors.indigo,
              leading: const Icon(Icons.home),
              title: const Text("Home"),
              onTap: () {
                Navigator.pop(context);
              },
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal:14),
            child: ListTile(
              iconColor: Colors.indigo,
              leading: const Icon(Icons.person),
              title: const Text("My Profile"),
              onTap: () {
                Navigator.pop(context);
              },
            ),
          ),
          const Padding(
            padding: EdgeInsets.symmetric(horizontal:14),
            child: ListTile(
              iconColor: Colors.indigo,
              leading: Icon(Icons.pages),
              title: Text("News"),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal:14),
            child: ListTile(
              iconColor: Colors.indigo,
              leading: const Icon(Icons.book),
              title: const Text("FAQ"),
              onTap: () {
                Navigator.pop(context);
              },
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 14),
            child: ListTile(
              iconColor: Colors.indigo,
              leading: const Icon(Icons.support),
              title: const Text("Support Desk"),
              onTap: () {
                //Add action
              },
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal:14),
            child: ListTile(
              iconColor: Colors.indigo,
              leading: const Icon(Icons.edit),
              title: const Text("Change PIN"),
              onTap: () {
                //Add Action
              },
            ),
          ),
          Expanded(child: Container()),
          SizedBox(
            width: 200,
            child: Container(
              decoration: BoxDecoration(color: Colors.indigo, borderRadius: BorderRadius.circular(25)),
              child: const ListTile(
                iconColor: Colors.white,
                leading: Icon(Icons.logout, color: Colors.white),
                title: Text("Logout", style: TextStyle(color: Colors.white)),
                contentPadding: EdgeInsets.only(left: 25),
                //onTap: onLogout,
              ),
            ),
          ),
          const SizedBox(height: 30)
        ],
      ),
    );
  }
}
